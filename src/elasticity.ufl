cell = tetrahedron
degree = 1
V = VectorElement("CG", cell, degree)

u = TrialFunction(V)
v = TestFunction(V)

u0, u1, u2 = split(u)
v0, v1, v2 = split(v)

x = SpatialCoordinate(cell)

f = as_vector([0, 0, 0])

youngs_modulus = 300
poisson_ratio = 0.3333

mu = youngs_modulus * poisson_ratio / ((1 + poisson_ratio) * (1 - 2* poisson_ratio))
lmbda = youngs_modulus / (2* (1 + poisson_ratio))

eps = sym(grad(v))
sigma = 2 * mu * sym(grad(u)) + lmbda * tr(sym(grad(u))) * Identity(3)

r = (inner(sigma, eps) - inner(f, v)) * dx

bctype = conditional(Or(x[2] < 1e-8, x[2] > 1 - 1e-8), 1, 0)

is_dirichlet = bctype, bctype, bctype
interpolate_expression = 0, 0, conditional(x[2] < 0.5, 0.0, -0.1)
